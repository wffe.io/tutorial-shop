import React, { useCallback } from 'react';
import { firebaseService } from 'services/firebase';
import { useAuthContext } from 'services';
import { Redirect } from 'react-router-dom';
import { Paper, makeStyles, Typography, Container } from '@material-ui/core';
import ReactGoogleButton from 'react-google-button';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    button: {
        marginTop: theme.spacing(2),
    },
}));

export function LoginPage() {
    const classes = useStyles();
    const { user } = useAuthContext();

    const signInWithGoogle = useCallback(() => {
        firebaseService.signInWithGoogle();
    }, []);

    return (
        <>
            {null !== user && <Redirect to="/" />}
            {null === user && (
                <Container maxWidth={'sm'}>
                    <Paper className={classes.paper}>
                        <Typography component="h1" variant="h5">
                            Sign in
                        </Typography>
                        <ReactGoogleButton onClick={signInWithGoogle} className={classes.button} />
                    </Paper>
                </Container>
            )}
        </>
    );
}
