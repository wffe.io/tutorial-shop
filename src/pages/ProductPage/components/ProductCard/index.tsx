import React, { useCallback } from 'react';
import { makeStyles, Paper, Typography, Button } from '@material-ui/core';
import { noop } from 'lodash';
import { Product, ProductId } from 'interfaces';
import { AddToCartButton, ProductAvatar } from 'components';

type ProductCallback = (id: ProductId) => void;

interface ProductCardProps extends Product {
    className?: string;
    onShowProductClick?: ProductCallback;
}

export function ProductCard({
    id,
    className,
    name,
    price,
    shortDescription,
    onShowProductClick = noop,
}: ProductCardProps) {
    const classes = useStyles();

    const handleShowProductClick = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();

            onShowProductClick(id);
        },
        [id, onShowProductClick],
    );

    return (
        <Paper onClick={handleShowProductClick} className={`${classes.container} ${className}`} elevation={3}>
            <ProductAvatar productId={id} />
            <Typography variant="h5" component="h2">
                {name}
            </Typography>
            <Typography>{shortDescription}</Typography>
            <Typography>Price: {price && price.toFixed ? price.toFixed(2) : '-'}</Typography>

            <Button onClick={handleShowProductClick}>Show product</Button>
            <AddToCartButton productId={id} />
        </Paper>
    );
}

const useStyles = makeStyles((theme) => ({
    container: {
        padding: theme.spacing(2),
        cursor: 'pointer',
    },
}));
