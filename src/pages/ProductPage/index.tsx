import React, { useCallback, useEffect } from 'react';
import { CircularProgress, Typography, Container, Paper, makeStyles, Button } from '@material-ui/core';
import { ProductId } from 'interfaces';
import { ProductCard } from './components/ProductCard';
import { useHistory } from 'react-router-dom';
import { useProducts } from 'services';

export function ProductPage() {
    const classes = useStyles();
    const history = useHistory();

    const { products, isLoading, fetchProducts } = useProducts();

    useEffect(fetchProducts, []);

    const handleShowProductClick = useCallback(
        (id: ProductId) => {
            history.push(`/product/${id}`);
        },
        [history],
    );

    const handleClickLoadMore = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();

            fetchProducts();
        },
        [fetchProducts],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Our products lorem
                </Typography>
                {products.length === 0 && <Typography>No products</Typography>}
                {products.length > 0 &&
                    products.map((product) => (
                        <ProductCard
                            {...product}
                            key={product.id}
                            className={classes.productCard}
                            onShowProductClick={handleShowProductClick}
                        />
                    ))}
                {isLoading && <CircularProgress />}
                <Button variant="contained" color="primary" disabled={isLoading} onClick={handleClickLoadMore}>
                    Load more...
                </Button>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    productCard: {
        marginBottom: theme.spacing(2),
        maxWidth: '300px',
    },
}));
