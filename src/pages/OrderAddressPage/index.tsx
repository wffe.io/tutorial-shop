import React, { useCallback, useEffect } from 'react';
import {
    Container,
    makeStyles,
    Paper,
    Typography,
    Button,
    CircularProgress,
    Grid,
    TextField,
    TextFieldProps,
} from '@material-ui/core';
import { useCartContext, formValidators } from 'services';
import { useHistory } from 'react-router-dom';
import { Form, FormRenderProps, Field, FieldRenderProps } from 'react-final-form';
import { Address } from 'interfaces';

export function OrderAddressPage() {
    const classes = useStyles();
    const history = useHistory();
    const { cart, setOrderAddress } = useCartContext();

    useEffect(() => {
        if (cart.length === 0) {
            history.goBack();
        }
    }, [cart, history]);

    const handleSubmit = useCallback(
        (values: Address) => {
            setOrderAddress(values);
            history.push('/order-confirmation');
        },
        [setOrderAddress, history],
    );

    const renderField = useCallback(
        (Component: React.ComponentType<TextFieldProps>) => ({
            input,
            meta,
            label,
        }: FieldRenderProps<Address[keyof Address]>) => {
            return (
                <Component
                    {...input}
                    label={label}
                    error={Boolean((meta.touched || meta.submitFailed) && meta.error)}
                    helperText={(meta.touched || meta.submitFailed) && meta.error ? meta.error : undefined}
                />
            );
        },
        [],
    );

    const handleFormRender = useCallback(
        ({ handleSubmit, submitting, pristine }: FormRenderProps<Address>) => {
            return (
                <form onSubmit={handleSubmit} className={classes.form}>
                    <Grid container>
                        <Grid item xs={12}>
                            <Field<Address['firstName']>
                                label="First Name"
                                name="firstName"
                                validate={formValidators.required}
                                render={renderField(TextField)}
                            />
                            <Field<Address['lastName']>
                                label="Last Name"
                                name="lastName"
                                validate={formValidators.required}
                                render={renderField(TextField)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field<Address['street']>
                                label="Street"
                                name="street"
                                validate={formValidators.required}
                                render={renderField(TextField)}
                            />
                            <Field<Address['buildingNumber']>
                                label="Building number"
                                name="buildingNumber"
                                validate={formValidators.required}
                                render={renderField(TextField)}
                            />
                            <Field<Address['flatNumber']>
                                label="Flat number"
                                name="flatNumber"
                                render={renderField(TextField)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <Field<Address['postalCode']>
                                label="Postal Code"
                                name="postalCode"
                                validate={formValidators.composeValidators(
                                    formValidators.required,
                                    formValidators.postalCode,
                                )}
                                render={renderField(TextField)}
                            />
                            <Field<Address['city']>
                                label="City"
                                name="city"
                                validate={formValidators.required}
                                render={renderField(TextField)}
                            />
                        </Grid>
                    </Grid>

                    <Button color="primary" variant="contained" type="submit" disabled={submitting || pristine}>
                        SUBMIT
                        {submitting && <CircularProgress />}
                    </Button>
                </form>
            );
        },
        [classes.form, renderField],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Please provide your order address
                </Typography>
            </Paper>

            <Paper>
                <Form<Address> onSubmit={handleSubmit} render={handleFormRender} />
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    form: {
        display: 'flex',
        flexDirection: 'column',
    },
}));
