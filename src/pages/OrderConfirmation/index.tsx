import React, { useCallback, useEffect } from 'react';
import { Button, Container, makeStyles, Paper, Typography } from '@material-ui/core';
import { useCartContext } from 'services';
import { useHistory } from 'react-router-dom';

export function OrderConfirmationPage() {
    const classes = useStyles();
    const { orderAddress, cart } = useCartContext();
    const history = useHistory();

    useEffect(() => {
        if (orderAddress === undefined || cart.length === 0) {
            history.goBack();
        }
    }, [history, orderAddress, cart]);

    const handlePlaceOrderClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();

            console.log('PLACE ORDER', orderAddress, cart);
        },
        [cart, orderAddress],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Order confirmation
                </Typography>
            </Paper>
            <Paper className={classes.paper}>{JSON.stringify(orderAddress)}</Paper>
            <Paper>
                <Button variant="contained" color="primary" size="large" onClick={handlePlaceOrderClick}>
                    PLACE ORDER
                </Button>
            </Paper>
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
}));
