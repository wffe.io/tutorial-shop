import React, { useCallback } from 'react';
import { Container, makeStyles, Paper, Typography, Button } from '@material-ui/core';
import { Cart } from 'components';
import { useCartContext } from 'services';
import { useHistory } from 'react-router-dom';

export function CartPage() {
    const classes = useStyles();
    const history = useHistory();
    const { cart } = useCartContext();

    const handleCheckoutClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();

            history.push('/order-address');
        },
        [history],
    );

    return (
        <Container maxWidth={'lg'}>
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h5" className={classes.title}>
                    Cart
                </Typography>
            </Paper>

            <Paper>
                <Cart isReadOnly={false} />
            </Paper>

            {cart.length > 0 && (
                <Paper className={classes.checkoutButtonContainer}>
                    <Button variant="contained" color="primary" size="large" onClick={handleCheckoutClick}>
                        CHECKOUT
                    </Button>
                </Paper>
            )}
        </Container>
    );
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(1),
        padding: theme.spacing(2),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
        marginBottom: theme.spacing(2),
    },
    checkoutButtonContainer: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(2),
        display: 'flex',
        justifyContent: 'center',
    },
}));
