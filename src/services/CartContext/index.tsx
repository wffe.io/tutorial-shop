import React, { createContext, useCallback, useContext, useMemo, useReducer } from 'react';
import { cond, constant, matches, noop, find } from 'lodash';
import { Address, Product, ProductId } from 'interfaces';
import { useProducts } from '../ProductContext';

type AddToCartAction = { type: 'ADD_TO_CART'; productId: ProductId };
type RemoveFromCartAction = { type: 'REMOVE_FROM_CART'; productId: ProductId };
type IncrementAction = { type: 'INCREMENT'; productId: ProductId };
type DecrementAction = { type: 'DECREMENT'; productId: ProductId };
type SetAmountAction = { type: 'SET_AMOUNT'; productId: ProductId; amount: number };
type SetOrderAddressAction = { type: 'SET_ORDER_ADDRESS'; orderAddress: Address };

type CartAction =
    | AddToCartAction
    | RemoveFromCartAction
    | IncrementAction
    | DecrementAction
    | SetAmountAction
    | SetOrderAddressAction;

interface CartState {
    readonly cart: Map<ProductId, number>;
    readonly orderAddress?: Address;
}

type CartContextType = {
    readonly cart: Array<{ product: Product; amount: number }>;
    readonly orderAddress?: Address;
    addToCart(id: ProductId): void;
    removeFromCart(id: ProductId): void;
    isInCart(id: ProductId): boolean;
    increment(id: ProductId): void;
    decrement(id: ProductId): void;
    setAmount(id: ProductId, amount: number): void;
    setOrderAddress(orderAddress: Address): void;
};

const CART_DEFAULT_CONTEXT = createContext<CartContextType>({
    cart: [],
    orderAddress: undefined,
    addToCart: noop,
    removeFromCart: noop,
    isInCart: () => false,
    increment: noop,
    decrement: noop,
    setAmount: noop,
    setOrderAddress: noop,
});

export function useCartContext() {
    return useContext(CART_DEFAULT_CONTEXT);
}

export function CartContextProvider({ children }: React.PropsWithChildren<{}>) {
    const [{ cart, orderAddress }, dispatch] = useReducer(cartReducer, {
        cart: new Map<ProductId, number>(),
        orderAddress: undefined,
    });
    const { products } = useProducts();

    const addToCart = useCallback(
        (id: ProductId) => {
            dispatch(addToCartAction(id));
        },
        [dispatch],
    );

    const removeFromCart = useCallback(
        (id: ProductId) => {
            dispatch(removeFromCartAction(id));
        },
        [dispatch],
    );

    const isInCart = useCallback((id: ProductId) => cart.has(id), [cart]);

    const increment = useCallback(
        (id: ProductId) => {
            dispatch(incrementAction(id));
        },
        [dispatch],
    );

    const decrement = useCallback(
        (id: ProductId) => {
            dispatch(decrementAction(id));
        },
        [dispatch],
    );

    const setAmount = useCallback(
        (id: ProductId, amount: number) => {
            dispatch(setAmountAction(id, amount));
        },
        [dispatch],
    );

    const setOrderAddress = useCallback(
        (orderAddress: Address) => {
            dispatch(setOrderAddressAction(orderAddress));
        },
        [dispatch],
    );

    const contextValue: CartContextType = useMemo(
        () => ({
            cart: [...cart.keys()]
                .map((productId) => ({
                    product: find(products, { id: productId }) as Product,
                    amount: cart.get(productId) || 0,
                }))
                .filter(({ product }) => Boolean(product)),
            addToCart,
            removeFromCart,
            isInCart,
            increment,
            decrement,
            setAmount,
            setOrderAddress,
            orderAddress,
        }),
        [
            cart,
            addToCart,
            removeFromCart,
            isInCart,
            increment,
            decrement,
            setAmount,
            products,
            setOrderAddress,
            orderAddress,
        ],
    );

    return <CART_DEFAULT_CONTEXT.Provider value={contextValue}>{children}</CART_DEFAULT_CONTEXT.Provider>;
}

function cartReducer(state: CartState, action: CartAction): CartState {
    return cond([
        [
            matches('ADD_TO_CART'),
            () => {
                const productId = (action as AddToCartAction).productId;

                return {
                    ...state,
                    cart: state.cart.has(productId)
                        ? state.cart
                        : new Map<ProductId, number>(state.cart).set(productId, 1),
                };
            },
        ],
        [
            matches('REMOVE_FROM_CART'),
            () => {
                const productId = (action as RemoveFromCartAction).productId;

                return {
                    ...state,
                    cart: state.cart.has(productId) ? removeItemFromCart(state.cart, productId) : state.cart,
                };
            },
        ],
        [
            matches('INCREMENT'),
            () => {
                const productId = (action as IncrementAction).productId;

                return {
                    ...state,
                    cart: state.cart.has(productId)
                        ? new Map<ProductId, number>(state.cart).set(productId, (state.cart.get(productId) || 0) + 1)
                        : state.cart,
                };
            },
        ],
        [
            matches('DECREMENT'),
            () => {
                const productId = (action as DecrementAction).productId;

                return {
                    ...state,
                    cart: state.cart.has(productId)
                        ? new Map<ProductId, number>(state.cart).set(
                              productId,
                              Math.max((state.cart.get(productId) || 0) - 1, 0),
                          )
                        : state.cart,
                };
            },
        ],
        [
            matches('SET_AMOUNT'),
            () => {
                const { productId, amount } = action as SetAmountAction;

                return {
                    ...state,
                    cart: state.cart.has(productId)
                        ? new Map<ProductId, number>(state.cart).set(productId, Math.max(amount, 0))
                        : state.cart,
                };
            },
        ],
        [
            matches('SET_ORDER_ADDRESS'),
            () => {
                const { orderAddress } = action as SetOrderAddressAction;

                return { ...state, orderAddress };
            },
        ],
        [() => true, constant(state)],
    ])(action.type);
}

function addToCartAction(productId: ProductId): AddToCartAction {
    return { type: 'ADD_TO_CART', productId };
}

function removeFromCartAction(productId: ProductId): RemoveFromCartAction {
    return { type: 'REMOVE_FROM_CART', productId };
}

function incrementAction(productId: ProductId): IncrementAction {
    return { type: 'INCREMENT', productId };
}

function decrementAction(productId: ProductId): DecrementAction {
    return { type: 'DECREMENT', productId };
}

function setAmountAction(productId: ProductId, amount: number): SetAmountAction {
    return { type: 'SET_AMOUNT', productId, amount };
}

function setOrderAddressAction(orderAddress: Address): SetOrderAddressAction {
    return { type: 'SET_ORDER_ADDRESS', orderAddress };
}

function removeItemFromCart(cart: Map<ProductId, number>, productId: ProductId): Map<ProductId, number> {
    const newMap = new Map<ProductId, number>(cart);
    newMap.delete(productId);
    return newMap;
}
