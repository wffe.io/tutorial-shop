export const formValidators = Object.freeze({
    required: (value: string | number | undefined) => (value ? undefined : 'This field is required'),
    postalCode: (value: string) => (/^[0-9]{2}-[0-9]{3}$/.test(value) ? undefined : 'Wrong Postal code. Format XX-XXX'),
    composeValidators: (...validators: Array<(value: string) => string | undefined>) => (value: string) =>
        validators.reduce((error: string | undefined, validator) => error || validator(value), undefined),
});
