import firebase from 'firebase';
import { FirebaseProduct } from '../interfaces';

const firebaseConfig = {
    apiKey: 'AIzaSyD4W4123Ld4iSJWWumFPra9qwLdVf9_LWI',
    authDomain: 'wffe-tutorial-shop.firebaseapp.com',
    databaseURL: 'https://wffe-tutorial-shop.firebaseio.com',
    projectId: 'wffe-tutorial-shop',
    storageBucket: 'wffe-tutorial-shop.appspot.com',
    messagingSenderId: '377514261892',
    appId: '1:377514261892:web:5f6f707b01b9e643bfb1d1',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseAuth = firebaseApp.auth();
const firestore = firebaseApp.firestore();

export const collections = Object.freeze({
    shopItems: firestore.collection('shopItems') as firebase.firestore.CollectionReference<FirebaseProduct>,
});

export const firebaseService = {
    async signInWithGoogle() {
        await firebaseAuth.useDeviceLanguage();
        await firebaseAuth.setPersistence(firebase.auth.Auth.Persistence.LOCAL);
        return await firebaseAuth.signInWithRedirect(new firebase.auth.GoogleAuthProvider());
    },

    getRedirectResult() {
        return firebaseAuth.getRedirectResult();
    },

    signOut() {
        return firebaseAuth.signOut();
    },

    onAuthStateChanged: firebaseAuth.onAuthStateChanged.bind(firebaseAuth),
};
