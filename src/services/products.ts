import { collections } from './firebase';
import { pick, partialRight, flow, defaults } from 'lodash';
import { FirebaseProduct, Product } from '../interfaces';
import firebase from 'firebase';

function fetchProducts(
    productSnapshot: firebase.firestore.QueryDocumentSnapshot<FirebaseProduct> | undefined | null,
    elementsOnPage = 10,
) {
    const collection = collections.shopItems.orderBy('name');
    if (productSnapshot) {
        return collection.startAfter(productSnapshot).limit(elementsOnPage);
    }
    return collection.limit(elementsOnPage);
}

export function getProducts(
    productSnapshot: firebase.firestore.QueryDocumentSnapshot<FirebaseProduct> | undefined | null,
    elementsOnPage = 10,
): Promise<[firebase.firestore.QueryDocumentSnapshot<FirebaseProduct>, Product[]]> {
    return fetchProducts(productSnapshot, elementsOnPage)
        .get()
        .then((data) => [data.docs[data.docs.length - 1], data.docs.map(parseProduct)]);
}

const COLUMNS_TO_PICK_FROM_FIREBASE = ['id', 'name', 'price', 'shortDescription', 'description', 'miniature', 'image'];
const DEFAULT_VALUES = {
    id: '',
    name: 'Default name',
    price: '-1',
    shortDescription: '',
    description: '',
    miniature: undefined,
    image: undefined,
};

function parseProduct(element: firebase.firestore.QueryDocumentSnapshot<FirebaseProduct>): Product {
    return flow([
        partialRight(pick, COLUMNS_TO_PICK_FROM_FIREBASE),
        partialRight(defaults, DEFAULT_VALUES),
        (e) => ({ ...e, price: Number.parseFloat(e.price.toString().replace(',', '.')) }),
        (e) => e as Product,
    ])({
        id: element.id,
        ...element.data(),
    });
}
