export interface Product {
    readonly id: ProductId;
    readonly name: string;
    readonly price: number;
    readonly shortDescription: string;
    readonly description: string;
    readonly miniature?: string;
    readonly image?: string;
}

export interface FirebaseProduct {
    readonly available: boolean;
    readonly description: string;
    readonly shortDescription: string;
    readonly dockey: string;
    readonly image?: string;
    readonly miniature?: string;
    readonly name: string;
    readonly price: string | number;
}

export type ProductId = string;

export interface Address {
    readonly firstName: string;
    readonly lastName: string;
    readonly street: string;
    readonly buildingNumber: string;
    readonly flatNumber: string;
    readonly postalCode: string;
    readonly city: string;
}
