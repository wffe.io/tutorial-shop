import React from 'react';
import { Route, RouteProps, Redirect } from 'react-router-dom';
import { useAuthContext } from 'services';

export function PrivateRoute({ children, ...rest }: RouteProps) {
    const { user } = useAuthContext();

    return (
        <Route
            {...rest}
            render={({ location }) =>
                null !== user ? (
                    children
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location },
                        }}
                    />
                )
            }
        />
    );
}
