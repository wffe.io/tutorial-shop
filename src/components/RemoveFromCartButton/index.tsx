import React, { useCallback } from 'react';
import { ProductId } from 'interfaces';
import { Delete } from '@material-ui/icons';
import { Button } from '@material-ui/core';
import { useCartContext } from 'services';

interface RemoveFromCartButtonProps {
    readonly productId: ProductId;
    readonly className?: string;
}

export function RemoveFromCartButton({ productId, className = '' }: RemoveFromCartButtonProps) {
    const { removeFromCart, isInCart } = useCartContext();

    const handleDeleteFromCartClick = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();

            removeFromCart(productId);
        },
        [productId, removeFromCart],
    );

    return (
        <Button className={className} onClick={handleDeleteFromCartClick} disabled={!isInCart(productId)}>
            <Delete />
        </Button>
    );
}
