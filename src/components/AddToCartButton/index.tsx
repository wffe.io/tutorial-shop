import React, { useCallback } from 'react';
import { ProductId } from 'interfaces';
import { AddShoppingCart } from '@material-ui/icons';
import { Button } from '@material-ui/core';
import { useCartContext } from 'services';

interface AddToCartButtonProps {
    readonly productId: ProductId;
    readonly className?: string;
}

export function AddToCartButton({ productId, className = '' }: AddToCartButtonProps) {
    const { addToCart, isInCart } = useCartContext();

    const handleAddCartClick = useCallback(
        (e: React.MouseEvent) => {
            e.preventDefault();
            e.stopPropagation();

            addToCart(productId);
        },
        [productId, addToCart],
    );

    return (
        <Button className={className} onClick={handleAddCartClick} disabled={isInCart(productId)}>
            <AddShoppingCart />
        </Button>
    );
}
