import React, { useCallback } from 'react';
import { ProductId } from 'interfaces';
import { TextField, Button, makeStyles } from '@material-ui/core';
import { Add, Remove } from '@material-ui/icons';
import { RemoveFromCartButton } from 'components';
import { useCartContext } from 'services';
import { trim, trimStart } from 'lodash';

interface CartItemControlsProps {
    readonly productId: ProductId;
    readonly amount: number;
}

export function CartItemControls({ productId, amount }: CartItemControlsProps) {
    const classes = useStyles();

    const { increment, decrement, setAmount } = useCartContext();

    const handleAddClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            increment(productId);
        },
        [increment, productId],
    );

    const handleSubtractClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            decrement(productId);
        },
        [decrement, productId],
    );

    const handleAmountChange = useCallback(
        (e: React.ChangeEvent<HTMLInputElement>) => {
            const trimmedStart = trimStart(e.currentTarget.value, '0');
            setAmount(productId, Number.parseInt(trim(trimmedStart, ' ')));
        },
        [productId, setAmount],
    );

    return (
        <>
            <Button onClick={handleAddClick}>
                <Add />
            </Button>
            <TextField value={amount} className={classes.input} onChange={handleAmountChange} type="number" />
            <Button onClick={handleSubtractClick}>
                <Remove />
            </Button>
            <RemoveFromCartButton productId={productId} />
        </>
    );
}

const useStyles = makeStyles((theme) => ({
    input: {
        width: '50px',
    },
}));
