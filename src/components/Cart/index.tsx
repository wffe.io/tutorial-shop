import React from 'react';
import { useCartContext } from 'services';
import { CartItemControls } from './components/CartItemControls';
import { Grid, Typography } from '@material-ui/core';
import { ProductAvatar } from '../ProductAvatar';

interface CartProps {
    readonly isReadOnly: boolean;
}

export function Cart({ isReadOnly }: CartProps) {
    const { cart } = useCartContext();

    if (cart.length === 0) {
        return <Typography>Cart is empty</Typography>;
    }

    return (
        <ul>
            {cart.map(({ product: { id, name }, amount }) => (
                <li key={id}>
                    <Grid container>
                        <Grid item>
                            <ProductAvatar productId={id} />
                        </Grid>
                        <Grid item>{name}</Grid>
                        <Grid item>{isReadOnly ? amount : <CartItemControls productId={id} amount={amount} />}</Grid>
                    </Grid>
                </li>
            ))}
        </ul>
    );
}
