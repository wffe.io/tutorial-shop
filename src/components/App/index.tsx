import React from 'react';
import { AppContexts, AppLayout, AppRoutes } from './components';

export function App() {
    return (
        <AppContexts>
            <AppLayout>
                <AppRoutes />
            </AppLayout>
        </AppContexts>
    );
}
