import React, { useCallback } from 'react';
import { Badge, IconButton } from '@material-ui/core';
import { ShoppingCart as ShoppingCartIcon } from '@material-ui/icons';
import { useCartContext } from 'services';

interface CartMenuButtonProps {
    onClick: () => void;
}

export function CartMenuButton({ onClick }: CartMenuButtonProps) {
    const { cart } = useCartContext();

    const handleClick = useCallback(
        (e: React.MouseEvent<HTMLButtonElement>) => {
            e.preventDefault();
            e.stopPropagation();

            onClick();
        },
        [onClick],
    );

    return (
        <IconButton aria-label="Cart" color="inherit" onClick={handleClick}>
            <Badge badgeContent={cart.length} color="secondary">
                <ShoppingCartIcon />
            </Badge>
        </IconButton>
    );
}
