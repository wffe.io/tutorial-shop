import React, { useCallback, useRef } from 'react';
import { IconButton, Menu, MenuItem, PopoverOrigin } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import { noop } from 'lodash';
import { firebaseService } from 'services';

const anchorOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'right',
};

const transformOrigin: PopoverOrigin = {
    vertical: 'top',
    horizontal: 'right',
};

interface UserMenuProps {
    readonly onMenuOpened?: () => void;
}

export function UserMenu({ onMenuOpened = noop }: UserMenuProps) {
    const iconRef = useRef<HTMLButtonElement>(null);
    const [isOpened, setIsOpened] = React.useState(false);

    const handleMenu = useCallback(() => {
        setIsOpened(true);
        onMenuOpened();
    }, [onMenuOpened]);

    const handleClose = useCallback(() => {
        setIsOpened(false);
    }, []);

    const handleLogout = useCallback(() => {
        handleClose();
        firebaseService.signOut();
    }, [handleClose]);

    return (
        <div>
            <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={handleMenu}
                color="inherit"
                ref={iconRef}
            >
                <AccountCircle />
            </IconButton>
            <Menu
                id="menu-appbar"
                anchorEl={iconRef.current}
                anchorOrigin={anchorOrigin}
                keepMounted
                transformOrigin={transformOrigin}
                open={isOpened}
                onClose={handleClose}
            >
                <MenuItem onClick={handleClose}>Profile</MenuItem>
                <MenuItem onClick={handleClose}>My account</MenuItem>
                <MenuItem onClick={handleLogout}>Logout</MenuItem>
            </Menu>
        </div>
    );
}
