import React, { useCallback } from 'react';
import {
    AppBar,
    Toolbar,
    Typography,
    Button,
    IconButton,
    Link,
    createStyles,
    makeStyles,
    Theme,
    MenuItem,
} from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';
import { UserMenu, CartMenuButton } from './components';
import { useAuthContext } from 'services';
import { useHistory } from 'react-router-dom';

export function TopBar() {
    const { user } = useAuthContext();
    const history = useHistory();

    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            menuButton: {
                marginRight: theme.spacing(2),
            },
            title: {
                flexGrow: 1,
            },
        }),
    );

    const classes = useStyles();

    const handleMenuClicked = useCallback(() => {
        console.log('Menu clicked!');
    }, []);

    const handleLoginButtonClick = useCallback(() => {
        history.push('/login');
    }, [history]);

    const handleCartButtonClick = useCallback(() => {
        history.push('/cart');
    }, [history]);

    const onMenuOpened = useCallback(() => {
        console.log('User menu opened');
    }, []);

    return (
        <AppBar position="static">
            <Toolbar>
                <IconButton
                    edge="start"
                    className={classes.menuButton}
                    color="inherit"
                    aria-label="menu"
                    onClick={handleMenuClicked}
                >
                    <MenuIcon />
                </IconButton>

                <Typography variant="h6" className={classes.title}>
                    <Link href={'/'} color="inherit">
                        Tutorial Shop
                    </Link>
                </Typography>

                {null !== user && (
                    <MenuItem>
                        <CartMenuButton onClick={handleCartButtonClick} />
                    </MenuItem>
                )}

                {null === user && (
                    <Button color="inherit" onClick={handleLoginButtonClick}>
                        Login
                    </Button>
                )}
                {null !== user && <UserMenu onMenuOpened={onMenuOpened} />}
            </Toolbar>
        </AppBar>
    );
}
