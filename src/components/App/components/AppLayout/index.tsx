import React from 'react';
import { Footer, TopBar } from './components';
import { Container, CssBaseline, makeStyles } from '@material-ui/core';

type AppLayoutProps = React.PropsWithChildren<{}>;

export function AppLayout({ children }: AppLayoutProps) {
    const classes = useStyles();

    return (
        <>
            <TopBar />
            <Container maxWidth="lg" component="main" className={classes.main}>
                <CssBaseline />
                {children}
                <Footer />
            </Container>
        </>
    );
}

const useStyles = makeStyles((theme) => ({
    main: {
        flexGrow: 1,
        flexShrink: 0,
    },
}));
