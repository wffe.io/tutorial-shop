import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { ProductPage, LoginPage, CartPage, OrderAddressPage, OrderConfirmationPage } from 'pages';
import { PrivateRoute } from 'components';

export function AppRoutes() {
    return (
        <Switch>
            <Route path="/" exact component={ProductPage} />
            <Route path="/login" component={LoginPage} />
            <PrivateRoute path="/cart" exact>
                <CartPage />
            </PrivateRoute>
            <PrivateRoute path="/order-address" exact>
                <OrderAddressPage />
            </PrivateRoute>
            <PrivateRoute path="/order-confirmation" exact>
                <OrderConfirmationPage />
            </PrivateRoute>
        </Switch>
    );
}
