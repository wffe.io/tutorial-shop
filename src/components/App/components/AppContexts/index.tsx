import React, { PropsWithChildren } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { AuthContextProvider, ProductContextProvider, CartContextProvider } from 'services';

type AppContextsProps = PropsWithChildren<{}>;

export function AppContexts({ children }: AppContextsProps) {
    return (
        <BrowserRouter>
            <AuthContextProvider>
                <ProductContextProvider>
                    <CartContextProvider>{children}</CartContextProvider>
                </ProductContextProvider>
            </AuthContextProvider>
        </BrowserRouter>
    );
}
