const fs = require('fs');
const admin = require('firebase-admin');
const csvtojsonV2 = require('csvtojson/v2');

const serviceAccount = require('./wffe-tutorial-shop-firebase-key.json');

const csvFilePath = process.argv[2];

if (!(csvFilePath && fs.existsSync(csvFilePath))) {
    console.error('No input file');
    process.exit(1);
}

getJsonFile(csvFilePath).then((data) => {
    sendDataToFirebase({
        collectionKey: 'shopItems',
        data,
        certificate: serviceAccount,
        databaseURL: 'https://wffe-tutorial-shop.firebaseio.com',
    });
});

async function getJsonFile(csvFilePath) {
    return await csvtojsonV2({
        delimiter: ';',
        colParser: {
            seed: 'omit',
            price: (value) => Number.parseFloat(value.replace(',', '.').replace(/ +/, '')),
            available: (value) => value == 1,
        },
    }).fromFile(csvFilePath);
}

function sendDataToFirebase({ collectionKey, data, certificate, databaseURL }) {
    admin.initializeApp({
        credential: admin.credential.cert(certificate),
        databaseURL,
    });

    const firestore = admin.firestore();
    const settings = { timestampsInSnapshots: true };
    firestore.settings(settings);
    if (data && typeof data === 'object') {
        Object.keys(data).forEach((docKey) => {
            firestore
                .collection(collectionKey)
                .doc(docKey)
                .set(data[docKey])
                .then((res) => {
                    console.log('Document ' + docKey + ' successfully written!');
                })
                .catch((error) => {
                    console.error('Error writing document: ', error);
                });
        });
    }
}
